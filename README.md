# Processamento de Imagens 2021: Projeto Final
## Processamento de Imagens do Projeto ENOÉ


### Resumo (prévia)
O projeto abordara o processamento digital de imagens aplicado ao _dataset_ do projeto ENOÉ, cujo seu objetivo é a detecção de cheias através do sensoriamento remoto por meio de captura de imagens de rios. Neste _dataset_ constam imagens referentes ao córrego (rio) de São Carlos. O objetivo é tratar as imagens para possibilitar a identificação do nível de água do rio e permitir o acompanhamento visual por especialistas (como meio de apoio). A utilização de técnicas de processamento de imagem como filtros, histograma, redução de ruído e resolução de imagem permitirão extrair informações precisas para a análise de cheia em rios por meio da aplicação em sistemas estruturados para previsão de enchentes (baseados em inteligência artificial), bem como reduzir a carga computacional e reduzir os tratamentos extras nas imagens de entrada destes sistemas. As imagens que constam no _dataset_ são provenientes de uma câmera local, a qual faz a captura de imagens no intervalo de tempo de cinco em cinco minutos ao logo do dia, armazenando imagens ao decorrer de 5 anos, de 2016 até 2021. 


### Objetivo Geral
Processar e tratar imagens pertencentes ao _datase_ do projeto ENOÉ a fim de apurar suas caracteristicas.


### Descrição das Imagens

#### Imagens de entrada
Todas as imagens selecionadas sofrem caracteristicas como: baixo contraste e realce positivo durante a captura noturna, além de baixo nível de saturação de brilho durante a captura diurna. Tendo como consequencia a tratamento dos demais atributos relacionadas a uma imagem.

As imagens estão disponíveis para consulta no _website_ [http://agora.icmc.usp.br:5001/](http://agora.icmc.usp.br:5001/), referente ao período de 2016 a 2021.


### Material e Métodos

#### Etapa 01

- Técnicas de processamento com Histogramas
    - Ajuste de brilho e contraste;
    - Equalização / Refinamento Imagem;
    - Segmentação;
    - Mapas de pseudocores (descartado em nosso caso de testes).
 
#### Etapa 02

- Detector de borda Canny
    - Redução de ruído por meio de convolução;
    - Cálculo do gradiente;
    - Non-Maximum Suppression;
    - Double Threshold;
    - Edge Tracking por Hyteresis.


## Imagens de exemplo 
As imagens abaixo serão utilizadas para os primeiros testes. 

Imagem de referência (retirada às 12:00 horas do dia):
![Imagem durante o dia](images/img0.jpg)

Imagem no começo da noite (retirada às 18:00 horas)
![alt text](images/img1.jpg)

Imagem da noite (retirada às 20:00 horas)
![alt text](images/img2.jpg)

Imagem da noite (retirada às 23:00 horas)
![alt text](images/img3.jpg)

## Os resultados obtidos
Através do processamento de dados foram obtidos resultados diversos. Onde podem
ser manipulados por meio da alteração de parâmetros existentes no método _Double_
_Threshold_. Assim obtendo um resultado que pode se adequar a aplicação, tal
gerado por meio da técnica de Canny. 

Onde neste trabalho foi codificado de forma segmentada, quebrado em partes, utilizando-se
da implementação existente na biblioteca SKImage apenas para conferência de resultados. 

Desta maneira, foi possível explorar, parte por parte, o conceito que Canny propunha
ao formular este método. 

No que diz respeito a Equalização de Histograma, as imagens obteram resultados 
satisfatórios na maioria dos casos, deixando brechas para serem tratadas novamente
em casos onde ainda continuam com alta e/ou baixa taxa de brilho e/ou constraste,
também gerando possíveis ruídos.

As imagens exibidas abaixo estão exibidas na sequência referente as imagens de exemplo
acima. 

### Equalização de Histograma
A equalização de histograma é capaz de acentuar detalhes não visiveis por meio da 
normalização da distribuição dos valores referentes a uma imagem.

Imagem de referência (retirada às 12:00 horas do dia):
![Imagem durante o dia](images/hist_equal/gray_scale_0.jpg)

Imagem no começo da noite (retirada às 18:00 horas)
![alt text](images/hist_equal/gray_scale_1.jpg)

Imagem da noite (retirada às 20:00 horas)
![alt text](images/hist_equal/gray_scale_2.jpg)

Imagem da noite (retirada às 23:00 horas)
![alt text](images/hist_equal/gray_scale_3.jpg)

### Canny
Filtro de Canny objetiva-se encontrar bordas por meio de convolução, empregando a primeira 
derivada para isto, suavizando ruídos e localizando as bordas (gradiente). 

Imagem de referência (retirada às 12:00 horas do dia):
![Imagem durante o dia](images/canny/canny_0.jpg)

Imagem no começo da noite (retirada às 18:00 horas)
![alt text](images/canny/canny_1.jpg)

Imagem da noite (retirada às 20:00 horas)
![alt text](images/canny/canny_2.jpg)

Imagem da noite (retirada às 23:00 horas)
![alt text](images/canny/canny_3.jpg)
