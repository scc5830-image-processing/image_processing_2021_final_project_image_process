# -*- coding: utf-8 -*-
"""trab_PID.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1Qzn7b4Vv21lPOiGGl-BfaJC0hSPiLV1J
"""

from google.colab import drive
drive.mount('/content/drive')

"""## The block of codes

* Nome: Angelo Victor Kraemer Foletto
  * Numero USP: 12620258
  *
  * Nome: Laleska Aparecida Ferreira Mesquita
  * Numero USP: 12116738
  *
  * Código do Curso: SCC5830 - Image Processing (2021) / 1
  * Docente: Moacir Antonelli Ponti
  * Assignment Final: Processamento de Imagens do Projeto ENOÉ
  * End Data: 07/07/2021
  * Create: 23/06/2021
  * Update: 13/07/2021
  *
  * https://gitlab.com/scc5830-image-processing/image_processing_2021_final_project_image_process

#### Imports
"""

## Imports
import os
import math
import argparse
import numpy as np
import random as rd
import imageio as imgio
import matplotlib.image as mpimg

from PIL import Image
from scipy import ndimage
from matplotlib import pyplot as plt

"""#### The gamma transformation"""

def _gamma(img, param):
    """
    Calc the gamma values.

    :param img: list()
    :param param: double
    :return: list()
    """
    return np.power(img / float(np.max(img)), param)

"""#### Histogram equalization
The feature normalization the bright and contrast of the image. Performed automatically through the probability of accumulated values. 
"""

def convert_to_gray(img):
  """
  Convert the image RGB to gray scale. This convert simplify the image 
  manipulation process.

  :param img: list()
  :return: list()
  """
  params = [0.299, 0.589, 0.114]
  swap = np.ceil(np.dot(img[..., :3], params))
  swap[swap > 255] = 255
  return swap


def _dictionary():
  """
  Dictionary: create a hash for storage the count of clone values.

  :return: hash table
  """
  swap = []

  for i in range(256):
    swap.append(str(i))
    swap.append(0)

  return {swap[i]: swap[i + 1] for i in range(0, len(swap), 2)}


def _discionary_values_intencity(img):
  """
  Dictionary: count the times as values of intencity appear in imagem.

  :param img: list()
  :return: hash table
  """
  disc = _dictionary()
  img[img > 255] = 255
  for row in img:
    for column in row:
      disc[str(int(column))] = disc[str(int(column))] + 1
  return disc


def _dictionary_probability(disc, shape):
  """
  Dictionary: count the probability this value appear.

  :param disc: hash table
  :param shape: integer
  :return: hash table
  """
  swap = {}
  for i in range(256):
    swap[str(i)] = disc[str(i)] / shape
  return swap


def _accumulated(disc_prob):
  """
  Get the accumulated probability for each value the histogram is sum to 
  accumulated probability the last interations.

  :param disc_prob: hash table
  :return: hash table
  """
  swap_acc = {}
  sum = 0

  for i in range(256):
    if i != 0:
      sum += disc_prob[str(i - 1)]
    swap_acc[str(i)] = disc_prob[str(i)] + sum
  return swap_acc


def _new_gray(hist_accum):
  """
  Get a new gray value for the pixel based in accumulated probability.

  :param hist_accum: hash table
  :return: hash table
  """
  swap = {}

  for i in range(256):
    swap[str(i)] = np.ceil(hist_accum[str(i)] * 255)
  return swap


def _new_img(img, new_gray_img):
  """
  Generate a new image.

  :param img: list()
  :param new_gray_img: hash table
  :return: list()
  """
  for row in range(img.shape[0]):
    for column in range(img.shape[1]):
      img[row][column] = new_gray_img[str(int(img[row][column]))]
  return img

"""### Canny

#### Noise Reduction - about convolution
"""

def convolution(img, masc, average=False):
  """
  The mask enter and walk into the image, multiplication the mask with the image

  :param img: list()
  :param masc: list()
  :param average: boolean
  :return: list()
  """
  if len(img.shape) == 3:
    img = convert_to_gray(img)

  img_row, img_col = img.shape
  masc_row, masc_col = masc.shape
  result = np.zeros(img.shape)
  pad_row = (masc_row - 1) // 2
  pad_col = (masc_col - 1) // 2
  padded_img = np.zeros((img_row + (2 * pad_row), img_col + (2 * pad_col)))
  padded_img[pad_row:padded_img.shape[0] - pad_row
             , pad_col:padded_img.shape[1] - pad_col] = img

  for row in range(img_row):
    for col in range(img_col):
      result[row, col] = np.sum(masc * padded_img[row:row + masc_row
                                                  , col:col + masc_col])
      if average:
        result[row, col] /= masc.shape[0] * masc.shape[1]
  return result
  
def normalize(masc, multipli, sigma):
  """
  Normalize the values of mask

  :param masc: list()
  :param multipli: float
  :param sigma: float
  :return: list()
  """
  return 1 / (np.sqrt(2 * np.pi) * sigma) * np.e ** (-np.power((masc - multipli) / sigma, 2) / 2)
 
def gaussianKernel(size, sigma=1):
  """
  Create a Gayssuab kernel (mask)
  
  :param size: integer 
  :param sigma: float
  :return: list()
  """
  masc_1D = np.linspace(-(size // 2), size // 2, size)
  for i in range(size):
    masc_1D[i] = normalize(masc_1D[i], 0, sigma)
  masc_2D = np.outer(masc_1D.T, masc_1D.T)

  masc_2D *= 1.0 / masc_2D.max() 
  return masc_2D
 
def gaussianBlur(image, masc_size):
  """
  Create a new image with blur (noise filter)
  
  :param image: list() 
  :param masc_size: integer
  :return: list()
  """
  return convolution(image, gaussianKernel(masc_size, sigma=math.sqrt(masc_size)), average=True)

"""#### Gradient Calculation"""

def sobelFilters(img):
  """
  The mask x and y convolution in original image to find the gradient.
  
  :param img: list() 
  :return: list(), list()
  """
  masc_x = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], np.float32)
  masc_y = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], np.float32)
  
  masc_x_conv = convolution(img, masc_x)
  masc_y_conv = convolution(img, masc_y)
  
  result = np.hypot(masc_x_conv, masc_y_conv)
  result = result / result.max() * 255
  theta = np.arctan2(masc_y_conv, masc_x_conv)
  
  return (result, theta)

"""#### Non-Maximum Suppression"""

def nonMaxSuppression(img, theta):
  """
  Apply this method to minimization the maximus values and create a refined lines.

  :param img: list()
  :param theta: list()
  :return: list()
  """
  pad_row, pad_col = img.shape
  result = np.zeros((pad_row,pad_col), dtype=np.int32)
  angulo = theta * 180. / np.pi
  angulo[angulo < 0]  += 180
  
  o22_5 = 22.5
  o67_5 = 67.5
  o112_5 = 112.5
  o157_5 = 157.5
  o180 = 180
  
  for i in range(1, pad_row - 1):
    for j in range(1, pad_col - 1):
      try:
        q = 255
        r = 255
        
        if (0 <= angulo[i, j] < o22_5) or (o157_5 <= angulo[i, j] <= o180):  #angulo 0
          q = img[i, j + 1]
          r = img[i, j - 1]
        elif (o22_5 <= angulo[i,j] < o67_5):  #angulo 45
          q = img[i + 1, j - 1]
          r = img[i - 1, j + 1]
        elif (o67_5 <= angulo[i,j] < o112_5):  #angulo 90
          q = img[i + 1, j]
          r = img[i - 1, j]
        elif (o112_5 <= angulo[i,j] < o157_5):  #angulo 135
          q = img[i - 1, j - 1]
          r = img[i + 1, j + 1]

        if (img[i, j] >= q) and (img[i, j] >= r):
          result[i, j] = img[i, j]
        else:
          result[i, j] = 0
      except:
        pass
  return result

"""#### Double threshold"""

def doubleThreshold(img, low_threshold_ratio=0.05, high_threshold_ratio=0.09):
  """
  Highlight the lines 
  
  :param img: list()
  :param low_threshold_ratio: float
  :param high_threshold_ratio: float
  :return: list()
  """
  high_threshold = img.max() * high_threshold_ratio;
  low_threshold = high_threshold * low_threshold_ratio;
  result = np.zeros((img.shape[0], img.shape[1]), dtype=np.int32)
  weak = np.int32(25)
  strong = np.int32(255)
  
  strong_i, strong_j = np.where(img >= high_threshold)
  weak_i, weak_j = np.where((img <= high_threshold) & (img >= low_threshold))
  
  result[strong_i, strong_j] = strong
  result[weak_i, weak_j] = weak
  
  return result, weak, strong

"""#### Edge Tracking by Hysteresis"""

def hysteresis(img, weak, strong=255):
  """
  This model permit the high threshold, make possible remove same noise but not 
  the edges.
  
  :param img: list()
  :param weak: integer
  :param strong: integer
  :return: list()
  """
  for i in range(1, img.shape[0] - 1):
    for j in range(1, img.shape[1] - 1):
      if (img[i,j] == weak):
        try:
          if ((img[i + 1, j-1] == strong) 
                or (img[i + 1, j] == strong) 
                or (img[i + 1, j + 1] == strong)
                or (img[i, j-1] == strong) 
                or (img[i, j + 1] == strong)
                or (img[i-1, j-1] == strong) 
                or (img[i-1, j] == strong) 
                or (img[i-1, j + 1] == strong)
              ):
            img[i, j] = strong
          else:
            img[i, j] = 0
        except:
          pass
  return img

"""### Filtering - Noise reduction about Mean values"""

def medianFilter(img, filter_weight=3):
  """
  Median filter select the median in 

  :return: new image array
  """
  shape = img.shape
  size_pad = int(filter_weight / 2)
  swap_img = np.pad(img
                    , [(size_pad, size_pad), (size_pad, size_pad)]
                    , 'constant'
                    , constant_values=(0, 0))
  size_matrix = swap_img.shape
  swap = np.zeros(shape, dtype=np.float64)
  
  for i in range(filter_weight, size_matrix[0]):
    swap_i = i - filter_weight
    for j in range(filter_weight, size_matrix[1]):
      swap_j = j - filter_weight
      swap[swap_i][swap_j] = np.sort(swap_img[swap_i:i, swap_j:j].flatten()
                                    )[int(np.power(filter_weight, 2) / 2)]
  return swap

"""### Utils"""

def random_img(path=None):
  """
  Choose a random image in the specified path.

  :param path: String
  :return: String
  """
  if path is None:
    return path
  return rd.choice(os.listdir(path))

def plot_hist(hist, hist2=False, size=(20, 10)):
  """
  Plot the histogram.
  
  :param hist: list()
  :param hist2: list() or False
  :param size: array()
  :return: None
  """
  if hist2 is False:
    plt.bar(hist.keys(), hist.values(), width=1)
    plt.xlabel("Níveis intensidade")
    ax = plt.gca()
    ax.axes.xaxis.set_ticks([])
    plt.grid(True)
    plt.show()
  else:
    figure, axarr = plt.subplots(1,2, figsize=size)
    axarr[0].bar(hist.keys(), hist.values())
    axarr[1].bar(hist2.keys(), hist2.values())

def plot_figures(fig1, fig2=False, size=(20, 10), color='gray'):
  """
  Plot the figures.  
  
  :param fig1: list()
  :param fig2: list()
  :param size: array()
  :param color: 
  :return: None
  """
  if fig2 is False:
    plt.imshow(fig1, color)
    plt.show()
  else:
    figure, axarr = plt.subplots(1,2, figsize=size)
    axarr[0].imshow(fig1, cmap=color)
    axarr[1].imshow(fig2, cmap=color)

"""### Main method
Call the necessary methods.
"""

def main(img, img_gray=False, gamma=False):
  """
  The main block.

  :param img: list()
  :param img_gray: list or False
  :param gamma: float
  :return: img_org     : list()
           , img_gray  : list()
           , new_img   : list()
           , hist_prob : list()
           , swap_prob : list()
           , hist_count: list()
           , swap_count: list()
           , hist_accum: list()
           , swap_accum: list()
           or False
  """
  try:
    img_org = imgio.imread(img)
    img_gray = convert_to_gray(img_org)
    if gamma is not False:
      img_gray = _gamma(img_gray, gamma)
    number_pixel = img_gray.shape[0] * img_gray.shape[1]

    hist_count = _discionary_values_intencity(img_gray)
    hist_prob = _dictionary_probability(hist_count, number_pixel)
    hist_accum = _accumulated(hist_prob)

    if img_gray is not False:
      new_img = _new_img(img_gray, _new_gray(hist_accum))
    else:
      new_img = imgio.imread(img_gray)
    swap_count = _discionary_values_intencity(new_img)
    swap_prob = _dictionary_probability(swap_count, number_pixel)
    swap_accum = _accumulated(swap_prob)

    return (
        img_org
        , img_gray
        , new_img
        , hist_prob
        , swap_prob
        , hist_count
        , swap_count
        , hist_accum
        , swap_accum
    )
  except RuntimeError as ex:
    print(img)
    raise IOError(ex)
  return False

"""# The run main block

Where initialize the variables and call the methods.

"""

# Static variables
path = '/content/drive/Shareddrives/PID_final_project/images'
path_orig = '20210524'
path_proc = 'test'

# Dynamic variables
img_main = list()
gamma = int()  # value bewteen 0 and 1

"""##### Proccess all imagens and save a external folder."""

# Select all images inside the path.
path_swap = '{}/{}'.format(path, path_orig)
img_names = os.listdir(path_swap)
print('Images to proccess: ', len(img_names))
percent = 100 / len(img_names)
percent_swap = float()
for i in img_names:
  percent_swap = percent_swap + percent
  print('Progress: %.2f %%' % percent_swap)
  swap = main('{}/{}'.format(path_swap, i))
  if swap is not False:
    img_main.append(swap)
    imgio.imwrite('{}/{}/gray_scale_{}'.format(path, path_proc, i), swap[2].astype(np.uint8))

"""###### Histogram
Plot the histogram before normalization for found the better bright and contrast.
"""

plot_hist(img_main[0][3])

plot_hist(img_main[0][4])

"""###### Figures
Show result after proccess imagens.
"""

# Original image | proccess image
plot_figures(img_main[0][0], img_main[0][1])

# Original image in gray scale | proccess image
plot_figures(img_main[0][1], img_main[0][2])

"""#### Proccess a single image
Get only one image to proccess.
"""

path_swap = '{}/{}'.format(path, path_orig)
img2proccess = random_img(path_swap)

img_main = main('{}/{}'.format(path_swap, img2proccess))
# imgio.imwrite('{}/{}/gray_scale_{}'.format(path, path_proc, i), swap[2].astype(np.uint8))

# This variable contant the return of "main" method. 
# The sequence is: img_org, img_gray, new_img, hist_prob, swap_new_img
# img

"""
  Run the four test images
"""
for i in range(4):
  path_swap = '{}/img{}.jpg'.format(path, i)

  img_main = main(path_swap)
  imgio.imwrite('{}/gray_scale_{}.jpg'.format(path, i), img_main[2].astype(np.uint8))

"""###### Histogram"""

"""
  The histogram of values intencity 
  left: original image
  right: new image
"""
plot_hist(img_main[5], img_main[6])

"""
 The histogram of probability
  left: original image
  right: new image
"""
plot_hist(img_main[3], img_main[4])

"""
  The histogram of accumulated
  left: original image
  right: new image
"""
plot_hist(img_main[7], img_main[8])

"""###### Figures"""

"""
  left: original image (RGB)
  right: new image
"""
plot_figures(img_main[0], img_main[2])

"""
  left: original image (gray)
  right: new image
"""
plot_figures(img_main[1], img_main[2])

"""#### Pseudocolor """

def pseudocolor(img, cmap='hot'):
  """
  Create a image about the pseudocolor (let or hot).
  
  :param img: list() 
  :param cmap: String - insert 'jet' or 'hot'
  :return: list()
  """
  cm_hot = mpl.cm.get_cmap(cmap)
  img_src = Image.open(img).convert('L')
  # img_src.thumbnail((1280,720))  # set size if necessary
  im = np.array(img_src)
  im = cm_hot(im)
  im = np.uint8(im * 255)
  im = Image.fromarray(im)
  # im.save('{}/test_jet.png'.format(path))  # To save the image
  return im

pseudo1 = pseudocolor('{}/{}'.format(path_swap, img2proccess), cmap='hot')
pseudo2 = pseudocolor('{}/{}'.format(path_swap, img2proccess), cmap='jet')

plot_figures(pseudo1, pseudo2)

"""#### Test noise reduction about Means values.

"""

"""
  Run to apply the Noise Reduction about Means values.
"""

valid_folder = True
count = 0

# Random choice
img_gray2proccess = random_img('{}/{}'.format(path, path_proc))
# Create folder
folder = '{}/{}/noise_reduction_{}'.format(path, path_proc, img_gray2proccess.split('.')[0])

try:
  os.mkdir(folder)
except:
  if len(os.listdir(folder)) != 0:
    valid_folder = False

if valid_folder:
  try:
    sigma = 20
    percent = 100 / (sigma // 2)
    percent_swap = float()
    for i in range(1, sigma, 2):
      percent_swap = percent_swap + percent
      print('Progress: %.2f %%' % percent_swap)
      filter_weight = i
      img_swap = imgio.imread('{}/{}/{}'.format(path, path_proc, img_gray2proccess))
      swap = medianFilter(img_swap, filter_weight)
      imgio.imwrite('{}/{}_gray_scale_test_filter_weight_{}.jpg'.format(folder, count, filter_weight), swap.astype(np.uint8))
      count += 1
  except:
    print('Error to try generate a noise reduction image')

"""#### Canny -> Run

##### Run the single image
"""

"""
  Noise Reduction
"""
path_swap = '{}/{}'.format(path, path_orig)
img2proccess = random_img(path_swap)
image = imgio.imread('{}/{}'.format(path_swap, img2proccess))

size_filter = 5 # Default values is 5
gaussian_blur = gaussianBlur(image, size_filter)

plot_figures(image, gaussian_blur)

"""
  Gradient Calculation (with local funcition)
"""
sobel_filters = sobelFilters(gaussian_blur)
plot_figures(sobel_filters[0], sobel_filters[1])

"""
  Non-Maximum Suppression
"""
non_max_suppression = nonMaxSuppression(sobel_filters[0], sobel_filters[1])
plot_figures(non_max_suppression)

"""
  Double threshold

  Default values
  -> low_threshold_ratio  = 0.05
  -> high_threshold_ratio = 0.09

"""
low_threshold_ratio  = 0.09
high_threshold_ratio = 0.3

double_threshold = doubleThreshold(non_max_suppression
                                  , low_threshold_ratio=low_threshold_ratio
                                  , high_threshold_ratio=high_threshold_ratio
                                  )

"""
  Hysteresis
"""
edge_hysteresis = hysteresis(double_threshold[0], double_threshold[1])
print(double_threshold[1], double_threshold[2])
plot_figures(double_threshold[0], edge_hysteresis)

"""##### Run to all imagens and save on folder."""

path_swap = '{}/{}'.format(path, path_orig)
img_names = os.listdir(path_swap)

print('Images to proccess: ', len(img_names))
percent = 100 / len(img_names)
percent_swap = float()
size_filter = 5 # Default values is 5

low_threshold_ratio  = 0.09
high_threshold_ratio = 0.3

for i in img_names:
  percent_swap = percent_swap + percent
  print('Progress: %.2f %%' % percent_swap)
  
  image = imgio.imread('{}/{}'.format(path_swap, i))

  """
    Noise Reduction
  """
  gaussian_blur = gaussianBlur(image, size_filter)
  # plot_figures(image, gaussian_blur)

  """
    Gradient Calculation (with local funcition)
  """
  sobel_filters = sobelFilters(gaussian_blur)
  # plot_figures(sobel_filters[0], sobel_filters[1])

  """
    Non-Maximum Suppression
  """
  non_max_suppression = nonMaxSuppression(sobel_filters[0], sobel_filters[1])
  # plot_figures(non_max_suppression)

  """
    Double threshold
  """
  double_threshold = doubleThreshold(non_max_suppression
                                    , low_threshold_ratio=low_threshold_ratio
                                    , high_threshold_ratio=high_threshold_ratio
                                    )

  """
    Hysteresis
  """
  edge_hysteresis = hysteresis(double_threshold[0], double_threshold[1])
  # print(double_threshold[1], double_threshold[2])
  # plot_figures(double_threshold[0], edge_hysteresis)

  imgio.imwrite('{}/cany/cany_{}'.format(path, i), edge_hysteresis.astype(np.uint8))

"""### SKImage
 This method is imported to comparation the SKImage method and Canny method written in this notebook.
"""

from skimage.feature import canny
edges = canny(convert_to_gray(image)/155.)
plot_figures(edges)